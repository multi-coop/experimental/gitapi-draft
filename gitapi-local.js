'use strict';

const app = require('./gitapi/api');
const routes = require('./gitapi/routes');
const config = require('./gitapi/config/config');

const port = config.port || 3000;

app.use(`/`, routes); // path must route to lambda
app.listen(port, () => console.log(`Local app listening on port ${port}!`));
