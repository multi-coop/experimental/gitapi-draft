const httpStatus = require('http-status');
// const logger = require('./config/logger');
const urlParser = require('../utils/urlParser');
const ApiError = require('../utils/ApiError');
const Joi = require('joi');

const len = async (df) => {
  // console.log('data.service > dataframes > len > df.toString() :', df.toString());
  const result = df.length;
  return result;
};

const processOptsQueryArg = (argString, defaultValue = 'hide') => {
  let optName
  let value

  if (argString) {
    const argArr = argString.split(':');
    optName = argArr[0];
    value = argArr[argArr.length - 1] || defaultValue;
    value = value === 'show';
  } else {
    value = defaultValue
  }

  return {
    opt: optName,
    value: value,
  }
};

const processQueryArgs = (argString, dfSchema, availableOperators, defaultOperator) => {
  const argArr = argString.split(':');

  const fieldName = argArr[0];
  // console.log('data.service > processQueryArgs > fieldName :', fieldName);

  const field = dfSchema.find(f => f.name === fieldName)
  // console.log('data.service > processQueryArgs > field :', field);
  const operator = Joi.string().valid(...availableOperators).default(defaultOperator).validate(argArr[1]);
  // console.log('data.service > processQueryArgs > operator :', operator);
  
  let value = argArr[argArr.length - 1];
  value = value.toLowerCase();
  // console.log('data.service > processQueryArgs > value :', value);

  return {
    field: field,
    operator: operator,
    value: value
  }
};

// TO DO ...
const search = async (df, dfSchema, searchers = []) => {
  // console.log('data.service > search > df.toString() :', df.toString());
  // console.log('data.service > search > dfSchema :', dfSchema);
  // console.log('data.service > search > searchers :', searchers);
  const result = df;
  const availableOperators = [
    'is',  // is equal to
    'has', // contains string
  ];

  searchers.length && searchers.forEach(searcher => {
    console.log('\ndata.service > search > searcher :', searcher);
    const arg = processQueryArgs(filter, dfSchema, availableOperators, 'has')
    console.log('data.service > search > arg :', arg);
  });
  return result;
};

const filter = async (df, dfSchema, filters = []) => {
  let results = df;
  const availableOperators = [
    'is',  // is equal to
    'not', // not equal to
    'has', // contains string
    'starts',  // starts with
    'ends',  // ends with
    'gt',  // greater than
    'gte',  // greater than or equal
    'lt',  // less than
    'lte',  // greater than or equal
  ];
  // console.log('data.service > filter > results.head(3).toString() :\n', results.head(3).toString());
  // console.log('data.service > filter > dfSchema :', dfSchema);
  // console.log('data.service > filter > filters :', filters);
  filters.length && filters.forEach(filter => {
    // console.log('\ndata.service > filter > filter :', filter);
    const arg = processQueryArgs(filter, dfSchema, availableOperators, 'is')
    // console.log('data.service > filter > arg :', arg);
    const fieldId = arg.field && arg.field.id;
    results = results.where((row) => {
      const rowVal = row[fieldId].toLowerCase();
      let bool;
      switch (arg.operator.value) {
        case 'is':
          bool = rowVal === arg.value;
          break
        case 'not':
          bool = rowVal !== arg.value;
          break
        case 'has':
          bool = rowVal.includes(arg.value);
          break
        case 'starts':
          bool = rowVal.startsWith(arg.value);
          break
        case 'ends':
          bool = rowVal.endsWith(arg.value);
          break
        case 'gt':
          bool = rowVal > arg.value;
          break
        case 'gte':
          bool = rowVal >= arg.value;
          break
        case 'lt':
          bool = rowVal < arg.value;
          break
        case 'lte':
          bool = rowVal <= arg.value;
          break
      }
      return bool
    }); // data-forge
  });
  // console.log('data.service > filter > results.head(5).toString() :\n', results.head(5).toString());
  return results;
};

const sort = async (df, dfSchema, sorters = {}) => {
  // console.log('data.service > dataframes > sort > df.toString() :', df.toString());
  // console.log('data.service > dataframes > sort > sorters :', sorters);
  let results = df;

  const availableOperators = [
    'asc',  // ascendant  : lower to greater
    'desc',  // descendant : greater to lower
  ];

  sorters.length && sorters.forEach(sorter => {
    console.log('\ndata.service > sorters > sorter :', sorter);
    const arg = processQueryArgs(sorter, dfSchema, availableOperators, 'asc')
    console.log('data.service > sort > arg :', arg);
    switch(arg.operator.value) {
      case 'asc':
        results = results.orderBy(item => item[arg.field.id])
        break
      case 'desc':
        results = results.orderByDescending(item => item[arg.field.id])
        break
    }
  });
  // console.log('data.service > sorters > result.at(1) :\n', results.at(1));
  // console.log('data.service > sorters > result.head(5).toString() :\n', results.head(5).toString());

  return results;
};

const calculate = async (df, dfSchema, calculators = []) => {

  // let results = df;

  const calculations = {};

  const availableTypes = [
    'integer',
    'number'
  ];
  const availableOperators = [
    'avg',  // average of all numeric values
    'count',  // count items
    'sum',  // sum of all numeric values
    'min',  // minimum of all numeric values
    'max',  // maximum of all numeric values
  ];

  calculators.length && calculators.forEach(calc => {
    // console.log('\ndata.service > calculate > calc :', calc);
    const arg = processQueryArgs(calc, dfSchema, availableOperators, 'sum')
    // console.log('data.service > calculate > arg :', arg);

    if (!availableTypes.includes(arg.field.type)) {
      throw new ApiError(httpStatus.BAD_REQUEST, `Field '${arg.field.name}' is not a numeric type : ${arg.field.type}`);
    }

    const serie = df.getSeries(arg.field.id);
    // console.log('data.service > calculate > serie.head(15).toString() :\n', serie.head(15).toString());
    
    // sanitize values:  filter only numeric values from serie
    // const filtered = serie.filter(salesFigure => typeof salesFigure === 'number');
    // console.log('data.service > calculate > filtered.head(15).toString() :\n', filtered.head(15).toString());

    const fieldCalcultations = {}
    switch(arg.operator.value) {
      case 'count':
        fieldCalcultations.count = serie.count();
        break
      case 'avg':
        fieldCalcultations.average = serie.average();
        break
      case 'sum':
        fieldCalcultations.sum = serie.sum();
        break
      case 'min':
        fieldCalcultations.min = serie.min();
        break
      case 'max':
        fieldCalcultations.max = serie.max();
        break
    }
    if (calculations[arg.field.name]) {
      calculations[arg.field.name] = { ...calculations[arg.field.name], ...fieldCalcultations }
    } else {
      calculations[arg.field.name] = fieldCalcultations;
    }
  });
  // console.log('data.service > calculate > calculations :', calculations);

  return calculations
};

const paginate = async (req, dataset, results) => {
  // const df = req.app.get('df');
  // console.log('data.service > dataframes > paginate > results.tail(1).toString() :\n', results.tail(1).toString());

  const df = dataset.df;

  // console.log('data.service > dataframes > paginate > df.tail(1).toString() :\n', df.tail(1).toString());
  const maxIndex = df.count() - 1; // data-forge
  // console.log('data.service > dataframes > paginate > maxIndex :', maxIndex);

  const pagesLen = Math.ceil(df.count() / req.query.limit) || 1; // data-forge
  const perPage = parseInt(req.query.limit, 10) || 3;
  // console.log('data.service > dataframes > paginate > pagesLen :', pagesLen);
  // console.log('data.service > dataframes > paginate > perPage :', perPage);

  let res = results;

  let page = parseInt(req.query.page, 10) || 1;
  let startIdx = (page - 1) * perPage;
  const endIdx = startIdx + perPage - 1;
  let locRowIndices = [startIdx, endIdx];

  // console.log('data.service > dataframes > paginate > page :', page);
  // console.log('data.service > dataframes > paginate > startIdx :', startIdx);
  // console.log('data.service > dataframes > paginate > locRowIndices :', locRowIndices);

  if (page > pagesLen) {
    page = pagesLen;
    startIdx = perPage * (pagesLen - 1);
    locRowIndices = [startIdx, maxIndex];
  }

  if (res.count() < req.query.limit) {
    page = 1;
  }

  // console.log('data.service > dataframes > paginate > page :', page);
  // console.log('data.service > dataframes > paginate > locRowIndices :', locRowIndices);
  // console.log('data.service > dataframes > paginate > res.tail(1).toString() :\n', res.tail(1).toString());

  res = res.between(locRowIndices[0], locRowIndices[1]);
  // console.log('data.service > dataframes > paginate > res :', res);
  // console.log('data.service > dataframes > paginate > end...');
  return {
    pg: page,
    pgLen: pagesLen,
    perPg: perPage,
    results: res,
  };
};

const restructureResp = async (req, dataset, results, onlyOne) => {
  const columns = dataset.columns;
  console.log('\ndata.service > restructureResp > columns :\n', columns);
  console.log('data.service > restructureResp > results :\n', results);
  let res = {};
  if (onlyOne) {
    // res = results;
    const keys = Object.keys(columns);
    console.log('data.service > restructureResp > keys :\n', keys);
    for (const k of keys) {
      res[columns[k]] = results[k]
    };
    console.log('data.service > restructureResp > res :\n', res);
  } else {
    res = results.renameSeries(columns).toArray();
  }
  return res;
};

const buildResp = async (req, options = {}) => {
  // console.log('data.service > dataframes > buildResp > df.toString() :', df.toString());
  console.log('\ndata.service > dataframes > buildResp > options :', options);
  
  // const serverUrl = req.app.get('serverUrl');
  const serverUrlObj = urlParser.getGitapiUrl(req);
  console.log(`data.service > buildResp > serverUrlObj :`, serverUrlObj);
  const serverUrl = serverUrlObj.serverUrl;

  const datasets = req.app.get('datasets');

  const request = {
    params: req.params,
    query: req.query,
  };
  // console.log('data.service > dataframes > buildResp > req :', req);
  // console.log('data.service > dataframes > buildResp > req.params :', req.params);
  const datasetName = req.params.datasetName;
  // console.log('data.service > dataframes > buildResp > datasetName :', datasetName);
  // console.log('data.service > dataframes > buildResp > req.query :', req.query);
  const dataset = datasets.find(d => d.name === datasetName);

  // const datasetIndex = parseInt(request.query.dataset) || 0 ;
  // const datasetIndex = parseInt(request.query.dataset) || 0 ;
  // console.log('data.service > dataframes > buildResp > datasetIndex :', datasetIndex);
  // const dataset = datasets[datasetIndex];

  const df = dataset.df;
  const dfSchema = dataset.dfSchema;
  const columns = dataset.columns;
  const dfMeta = {
    help: "Dataset infos",
    index: dataset.index,
    name: dataset.name,
    // url: `${serverUrl}${req.baseUrl}${req._parsedUrl.pathname}`,
    url: `${serverUrl}${req._parsedUrl.pathname}`,
    meta: dataset.meta
  };
  // console.log('data.service > dataframes > buildResp > dfSchema :', dfSchema);

  let pagination;
  let count;
  let searchers;
  let filters;
  let sorters;
  let calculators;
  let calculations;

  const totalItems = df.count(); // data-forge
  // console.log('data.service > dataframes > buildResp > totalItems :', totalItems);

  let results = df;

  const searchRowById = req.params.rowId;
  
  if (searchRowById) {
    const rowId = searchRowById && parseInt(searchRowById)
    // search by row ID
    results = rowId > 0 && df.at(rowId);
    console.log('data.service > dataframes > buildResp > results :', results);
    pagination = {
      perPg: 1,
      pageNumber: 1,
      totalPAges: 1,
    };
    count = results ? 1 : 0;
  } else {
    // search
    searchers = req.query.q;
    searchers = typeof searchers === 'string' ? [searchers] : searchers;
    // console.log('data.service > dataframes > buildResp > searchers :', searchers);
    results = await search(results, dfSchema, searchers);

    // filter
    filters = req.query.filter;
    filters = typeof filters === 'string' ? [filters] : filters;
    // console.log('data.service > dataframes > buildResp > filters :', filters);
    results = await filter(results, dfSchema, filters);
    // console.log('data.service > dataframes > buildResp > results filters :\n', results.toString());

    // sort
    sorters = req.query.sortby;
    sorters = typeof sorters === 'string' ? [sorters] : sorters;
    // console.log('data.service > dataframes > buildResp > sorters :', sorters);
    results = await sort(results, dfSchema, sorters);

    // aggregate / calculate
    calculators = req.query.calc;
    // console.log('data.service > dataframes > buildResp > calculators :', calculators);

    // paginate results
    if (calculators) {
      calculators = typeof calculators === 'string' ? [calculators] : calculators;
      calculations = await calculate(results, dfSchema, calculators);
    } else {
      pagination = await paginate(req, dataset, results);
      results = pagination.results;
      // count 
    }
    count = results.count();
  };
  
  // rebuild response with original columns
  results = await restructureResp(req, dataset, results, searchRowById);
  // console.log('data.service > dataframes > buildResp > results :\n', results.toString());

  // process options
  const showOptionsConfig = [
    { opt: 'infos', default: 'show' },
    { opt: 'schema', default: 'hide' },
    { opt: 'columns', default: 'hide' },
    { opt: 'request', default: 'show' },
    { opt: 'meta', default: 'show' },
  ];
  const showOptionsObj = showOptionsConfig.reduce( (acc, v) => ({ ...acc, [v.opt]: v.default === 'show' }), {});
  // console.log('data.service > dataframes > buildResp > showOptionsObj (default) :', showOptionsObj);

  let showOptions = req.query.opt;
  showOptions = typeof showOptions === 'string' ? [showOptions] : showOptions;
  // console.log('data.service > dataframes > buildResp > showOptions :', showOptions);
  showOptions && showOptions.forEach(optStr => {
    let optObj = processOptsQueryArg(optStr);
    // console.log('data.service > dataframes > buildResp > optObj :', optObj);
    showOptionsObj[optObj.opt] = optObj.value
  })
  // console.log('data.service > dataframes > buildResp > showOptionsObj (parsed) :', showOptionsObj);

  // build response
  const data = {
    data: undefined,
    // infos: {
    //   result: count, // data-forge
    //   total: totalItems,
    //   limit: pagination && pagination.perPg,
    //   pageNumber: pagination && pagination.pg,
    //   totalPages: pagination && pagination.pgLen,
    // },
    // request: { ...request },
    // // schema: req.app.get('schema'), // data-forge
    // schema: dfSchema, // data-forge
    // columns: columns,
    // meta: dfMeta,
    // serverUrlObj: serverUrlObj,
  };

  if (calculators) {
    data.data = calculations;
  } else {
    data.data = results;
  };

  showOptionsConfig.forEach(option => {
    if (showOptionsObj[option.opt]) {
      switch(option.opt) {
        case 'infos':
          data.infos = {
              result: count, // data-forge
              total: totalItems,
              limit: pagination && pagination.perPg,
              pageNumber: pagination && pagination.pg,
              totalPages: pagination && pagination.pgLen,
            }
          break
        case 'schema':
          data.schema = dfSchema
          break
        case 'columns':
          data.columns = columns
          break
        case 'request':
          data.request = { ...request }
          break
        case 'meta':
          data.meta = dfMeta
          break
      }
    }
  });

  return data;
};

module.exports = {
  len,
  search,
  filter,
  sort,
  paginate,
  restructureResp,
  buildResp,
};
