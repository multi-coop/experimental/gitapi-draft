
const getGitapiUrl = (req) => {
  // console.log(`urlParser > getGitapiUrl > req :`, req);
  // console.log(`urlParser > getGitapiUrl > req.url :`, req.url);
  // console.log(`urlParser > getGitapiUrl > req.baseUrl :`, req.baseUrl);
  // console.log(`urlParser > getGitapiUrl > req.originalUrl :`, req.originalUrl);
  // console.log(`urlParser > getGitapiUrl > req.path :`, req.path);
  const env = req.app.get('env');
  const protocol = req.protocol;
  const host = req.get('host');
  const baseUrl = req.baseUrl;

  // const appendNetlify = env === 'development' ? '' : req.app.get('netlify_lambda');
  const serverUrl = `${protocol}://${host}${baseUrl}`;

  return {
    env: env,
    protocol: protocol,
    host: host,
    url: req.url,
    baseUrl: baseUrl,
    originalUrl: req.originalUrl,
    path: req.path,
    serverUrl: serverUrl
  }
};

module.exports = {
  getGitapiUrl
}