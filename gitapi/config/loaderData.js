const dataForge = require('data-forge');
// const logger = require('./logger');
const readSources = require ('../utils/sourceData');

// Build dataframe
const buildData = (source) => {
  // console.log('loaderData.js > buildData > source :\n', source);
  const data = source.data;
  const columns = source.columns;
  const schema = source.schema;
  const fields = schema.fields;
  // console.log('loaderData.js > buildData > columns :\n', columns);
  // console.log('loaderData.js > buildData > schema :\n', schema);

  // data-forge > dataframe
  let df = new dataForge.DataFrame(data);
  // console.log('\nloaderData.js > buildData > df.head(3).toString() :\n', df.head(3).toString());
  // console.log('... \n');
  
  // data-forge > dataset schema
  let dfSchema = df.getColumns().toArray().map((col, i) => {
    // console.log('\nloaderData.js > buildData > col.name :', col.name);
    const label = columns[col.name].trim();
    // console.log('loaderData.js > buildData > label :', label);
    const field = fields.find(f => f.name === label);
    // console.log('loaderData.js > buildData > field :\n', field);
    return {
      id: String(i),
      index: parseInt(col.name),
      type: field ? field.type : 'string',
      autoType: col.type,
      name: label,
    }
  });
  dfSchema = dfSchema.map(f => {
    if (f.type != f.autoType) {
      // do something on df
      const idx = f.index.toString()
      let colSerie = df.getSeries(idx);
      // convert type
      colSerie = colSerie.select(value => {
        let val = value;
        switch(f.type) {
          case 'number':
            val = Number(val) || null
            break;
          case 'integer':
            val = parseInt(val) || null
            break;
          case 'boolean':
            valLow = val.trim().toLowerCase();
            if (valLow === 'true' || valLow === 'false') {
              val = (valLow === 'true')
            } else {
              val = false
            }
            break;
        } 
        return val
      })
      df = df.withSeries(idx, colSerie)
    }
    delete f.autoType;
    delete f.index;
    return { ...f }
  })
  // console.log('loaderData.js > buildData > dfSchema (2) :\n', dfSchema);
  // console.log('... \n');
  
  return {
    index: source.index,
    name: source.name,
    meta: source.meta,
    df: df,
    dfSchema: dfSchema,
    columns: columns,
  }
};

// RETRIEVE DATA CSV
const readSrcs = async (dataSources) => {
  const sources = await readSources.readSourceFiles(dataSources);
  // console.log('\nloaderData.js > readSrcs >  sources :\n', sources);

  // loop sources
  const datasets = []
  sources.forEach(src => {
    // console.log('\nloaderData.js > readSrcs > src :\n', src);
    const data = buildData(src);
    datasets.push(data);
  });

  return datasets
} 

module.exports = {
  data: async (dataSources) => {
    return await readSrcs(dataSources);
  },
}