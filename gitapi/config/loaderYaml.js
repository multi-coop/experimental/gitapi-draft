const config = require('./config');
const logger = require('./logger');
const readSources = require ('../utils/sourceData');

const yamlFile = config.files || './config-gitapi.yaml';
// logger.info(`loader.js > yamlFile : ${yamlFile}`);

// For testing purposes
// const getAsyncData = async (val) => {
//   const res = await new Promise((resolve) => setTimeout(() => {
//     resolve()
//   }, 100));
//   return val
// };

// Read config file
const yamlData = readSources.readYamlFile(yamlFile); // return a Promise

module.exports = {
  // yamlFile: async () => {
  //   return await getAsyncData(yamlFile);
  // },
  yamlFile: yamlFile,
  yaml: async () => {
    return await yamlData;
  },
}