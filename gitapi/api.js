const express = require("express");
const cors = require('cors');
const serverless = require("serverless-http");
const bodyParser = require('body-parser');
const config = require('./config/config');
// const logger = require('./config/logger');
const loaderYaml = require('./config/loaderYaml');
const loaderData = require('./config/loaderData');

const netlifyLambda = `/.netlify/functions/api`

// Setting up app
const app = express();
const routes = require('./routes');
app.use(netlifyLambda, routes); // path must route to lambda

// enable cors
app.use(cors());
app.options('*', cors());
app.use(bodyParser.json())

// Set yaml content
app.set('yamlFile', loaderYaml.yamlFile);
app.set('yamlFile_loaded', true);
loaderYaml.yaml().then(yaml => {
  // console.log('api.js > (1) > yaml :\n', yaml);
  const dataSources = yaml.files.filter(f => !f.ignore);
  app.set('repo', yaml.repo);
  app.set('yaml', dataSources);
  app.set('yaml_loaded', true);

  const NODE_ENV = process.env.NODE_ENV;
  app.set('env', NODE_ENV )
  app.set('netlify_lambda', netlifyLambda )

  // console.log('api.js > (1) > dataSources :\n', dataSources);
  loaderData.data(dataSources).then(res => {
    // console.log('api.js > (2) > res :\n', true);
    app.set('datasets', res);
    app.set('datasets_loaded', true);
  })
});

module.exports = app;
module.exports.handler = serverless(app);
