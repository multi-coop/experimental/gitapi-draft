const express = require('express');
const urlParser = require('../utils/urlParser');

const router = express.Router();
const mainEndpoint = 'v1'

router.get('/', (req, res) => {
  // const request = {
  //   query: req.query,
  //   params: req.params,
  // };

  const meta = {
    yamlFile: req.app.get('yamlFile'),
    yaml: req.app.get('yaml'),
  };
  console.log(`test.route > '/' > meta :`, meta);

  // const serverUrl = req.app.get('serverUrl');
  const serverUrlObj = urlParser.getGitapiUrl(req);
  console.log(`test.route > '/' > serverUrlObj :`, serverUrlObj);
  const serverUrl = serverUrlObj.serverUrl;

  const availableDatasets = meta.yaml.map((f,i) => {
    const url = `${serverUrl}/${mainEndpoint}/${f.name || i}`
    return {
      index: i,
      name: f.name || String(i),
      file: f.file,
      url: url,
      test_url: `${url}?page=1&limit=3`
    }
  });

  res.json({
    title: 'GITAPI PROJECT (draft)',
    author: 'https://multi.coop',
    purpose: 'Expose csv files hosted on a repo',
    infos: 'Backed with serverless-http, express, and Netlify lambda functions',
    repo: req.app.get('repo'),
    env: req.app.get('env'),
    serverUrl: serverUrl,
    mainEndpoint: `${serverUrl}/${mainEndpoint}`,
    meta: meta,
    more: {
      infos: 'Discover the available datasets exposed with Gitapi on those links',
      // serverUrlObj: serverUrlObj,
      datasets: availableDatasets,
    }
  });
});

module.exports = router;
