const express = require('express');
const dataController = require('../controllers/data.controller');

const router = express.Router();

// root
router
  .route('/')
  .get(dataController.getData);

// by dataset
router
  .route('/:datasetName')
  .get(dataController.getDatasetByName);

router
  .route('/:datasetName/:rowId')
  .get(dataController.getDatasetByName);

module.exports = router;
