const express = require('express');
const testRoute = require('./test.route');
const dataRoute = require('./data.route');
// const docsRoute = require('./docs.route');
const config = require('../config/config');

const router = express.Router();

const defaultRoutes = [
  {
    path: '/',
    route: testRoute,
  },
  {
    path: '/v1',
    route: dataRoute,
  },
  // {
  //   path: '/docs',
  //   route: docsRoute,
  // },
];

const devRoutes = [
  // routes available only in development mode
  // {
  //   path: '/docs',
  //   route: docsRoute,
  // },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

/* istanbul ignore next */
if (config.env === 'development') {
  devRoutes.forEach((route) => {
    router.use(route.path, route.route);
  });
}

module.exports = router;
