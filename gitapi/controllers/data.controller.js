const httpStatus = require('http-status');
// const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const urlParser = require('../utils/urlParser');
const logger = require('../config/logger');
const { dataService } = require('../services');

// ---------------------- //
// Endpoint /v1 root
// ---------------------- //
const getData = catchAsync(async (req, res) => {
  // const serverUrl = req.app.get('serverUrl');
  const serverUrlObj = urlParser.getGitapiUrl(req);
  // console.log(`data.controller > getData > serverUrlObj :`, serverUrlObj);
  const serverUrl = serverUrlObj.serverUrl;

  const datasets = req.app.get('datasets');
  // const filter = pick(req.query, ['name', 'role']);
  // const options = pick(req.query, ['sortBy', 'limit', 'page']);
  // logger.info(`data.controller > getData > filter :\n${JSON.stringify(filter, null, 2)} `);
  // logger.info(`data.controller > getData > options :\n${JSON.stringify(options, null, 2)} `);
  // console.log(`data.controller > getData > req.app.get('config') : \n`, req.app.get('config'));

  // const result = await dataService.buildResp(req, {});
  const availableDatasets = datasets.map(dataset => {
    const dName = dataset.name
    const dIndex = dataset.index
    return {
      index: dIndex,
      name: dName,
      url: `${serverUrl}/${dName}`,
      meta: dataset.meta,
    }
  });
  const result = {
    help: "Here is the list of available datasets on the '/v1' endpoint",
    available_datasets: availableDatasets,
    // serverUrlObj: serverUrlObj, 
  };
  res.send(result);
});

// ---------------------- //
// Endpoint /v1 : by dataset
// ---------------------- //
const getDatasetByName = catchAsync(async (req, res) => {
  console.log()
  // logger.info(`data.controller > getDatasetByName > req.params : ${JSON.stringify(req.params)}`);
  // logger.info(`data.controller > getDatasetByName > req.query : ${JSON.stringify(req.query)}`);

  const options = {
    // searchId: parseInt(req.params.id),
  };
  const result = await dataService.buildResp(req, options);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Dataset not found');
  }
  res.send(result);
});

// --------------------------- //
// Endpoint /v1 : by dataset + row 
// --------------------------- //
const getDatasetRow = catchAsync(async (req, res) => {
  // logger.info(`data.controller > getDatasetRow > req.params : ${JSON.stringify(req.params)}`);
  const options = {};

  const result = await dataService.buildResp(req, options);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Dataset or dataset row not found');
  }
  res.send(result);
});

module.exports = {
  getData,
  getDatasetByName,
  getDatasetRow,
};
