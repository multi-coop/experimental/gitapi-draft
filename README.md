# <img src="/statics/gitapi-logo.png"  width="60" height="60" style="display: inline; margin-bottom: -.5em; margin-right: .3em"> GITAPI LIGHT (netlify + express)


[![Netlify Status](https://api.netlify.com/api/v1/badges/fc974f6e-3841-45ba-85e9-e1ea38187ea0/deploy-status)](https://app.netlify.com/sites/gitapi-draft/deploys) 

![](https://img.shields.io/gitlab/license/45341092) ![](https://img.shields.io/gitlab/issues/open-raw/45341092)


---

**A minimalist serverless REST API service, to expose data from csv file(s) from a repo, and deployable for almost no cost on Netlify.**

---

An open source project by the tech coop [multi](https://multi.coop)

---

## Demo

Online demo : https://gitapi-draft.netlify.app/

_**Note** : reload the page once or twice if you got `Error 500 : internal error`. You will see in below free serverless Netlify lambda functions [have limitations](#troubleshooting)_

---

## Quickstart

```bash
git clone https://gitlab.com/multi-coop/experimental/gitapi-draft
cd gitapi-draft
npm i
npm run dev
```

Run on : 

- `https://localhost:3000`
- `https://localhost:3000/v1`
- `https://localhost:3000/v1/0?page=2&limit=3`

---

## Why Gitapi ?

**Who does really like, or have the time, and have the money, to configure NGINX + Docker to deploy a REST API serving only a dozen/hundreds of items ?**

For small databases or for small projects you may need (or want) to dynamically request facets of some csv files you are working with, so you need a simple REST API : **you may need to "API-fy" your data**.

Building an API server is quite simple indeed with today's frameworks such as [FastApi](https://fastapi.tiangolo.com/), [Express](https://expressjs.com/), [Flask Restful](https://flask-restful.readthedocs.io/en/latest/), etc... 

But there's a but : deploying the very same REST API service is sometimes painfull and not cheap enough for such modest needs. Usually you would need to deploy your small REST API project on Heroku, AWS, Digital Ocean, set up your NGINX layers and so on... All of the above may be over complicated or not cheap enough given the size of your csv data.

**Gitapi aims to provide a very light nodejs REST API tool you can deploy for almost no cost at all on Netlify, performant enough to parse your csv data, and still keeping its database up to date with your repo's csv files !**

---

## How does it work ?

Gitapi is a simple [serverless](https://www.redhat.com/en/topics/cloud-native-apps/what-is-serverless) Node Express REST API you can deploy on [Netlify](https://www.netlify.com/) to expose csv data stored on the very same repo. In this repo the `/data` folder represents what you may start with.

Gitapi transforms csv files contained in your data folder into Dataframes and thus can expose API endpoints you can use to request facets of your csv file(s).

When deployed on Netlify the `/data` folder and the `*.yaml` files are automatically copied in Netlify thanks to the `netlify.toml` settings.

When deployed Gitapi gets the `config-gitapi.yaml` file (the default one) and parse it to check wich files it should expose.

Example of `config-gitapi.yaml` file : 

```yaml
# config-gitapi.yaml file

repo: https://gitlab.com/multi-coop/experimental/gitapi-draft
deploy: https://gitapi-draft.netlify.app/.netlify/functions/api
files:                                    <-- list of files
  - file: ./data/csv/test.csv             <-- csv file to expose
    name: test-file                       <-- dataset name ( default index number)             
    schema: ./data/json/test-schema.json  <-- csv schema to use
    ignore: false                         <-- use this dataset or don't
    options: 
      separator: ","
      ...
```

Then for each file Gitapi loads the csv file and transform them into `data-forge`'s Dataframe, and also uses its corresponding schema (respecting [TableSchema standards](https://specs.frictionlessdata.io/table-schema/)).

Those Dataframes are finally declared as global variables express, thus directly requestable in the API endpoints.

---

## Stack

- [*Express*](https://expressjs.com/) : light node.js server ;
- [*serverless-http*](https://github.com/dougmoscrop/serverless-http) : serverless wrapper for node.js ; 
- [*data-forge*](https://github.com/data-forge/data-forge-ts) : a javascript equivalent of Python's [Pandas](https://pandas.pydata.org/) ;
- [*netlify-lambda*](https://github.com/netlify/netlify-lambda) : builder for netlify lambda functions.

---

## Add Gitapi into a project

If you already have a project containing datasets, just import the Gitapi executable files (not the git history). 

```bash
npx install-gitapi
npm i
```

The `npx` module source code is [here](https://gitlab.com/multi-coop/experimental/install-gitapi.git).

### Test it locally 

Update your `config-gitapi.yaml` file and run :

```bash
npm run dev
```

---

## Local development

### Install Gitapi and its dependencies

```bash
git clone https://gitlab.com/multi-coop/experimental/gitapi-draft
cd gitapi-draft
npm i
```

### Run local server

```
npm run dev
```

Run on `https://localhost:3000`

- test route : `https://localhost:3000`
- API V1 : `https://localhost:3000/v1`

---

## Deploy on Netlify

Create a new site as usual on Netlify by pointing to your repo containing Gitapi source and your data. 

Beware having a correct `config-gitapi.yaml` file at the root of the repo.

### In settings

This part is automatically filled in Netlify thanks to the `netlify.toml` file, but you can check the following parameters when you create your Gitapi site in Netlify : 

| Parameter         | Value           |
| ---               | ---             |
| Build command     | `npm run build` | 
| Publish directory | `dist/`         |

### Deployment

You will be able to check your Gitapi site on the following urls :

- `https//<DEPLOY_SITE_NAME>.netlify.app` : corresponds to the `./dist/index.html` file
- `https//<DEPLOY_SITE_NAME>.netlify.app/.netlify/functions/api`
- `https//<DEPLOY_SITE_NAME>.netlify.app/.netlify/functions/api/v1`

### Note about Netlify deployment

You will see in the `/src/api.js` that API routes are served on : 

- `https//<DEPLOY_SITE_NAME>.netlify.app/.netlify/functions/api`

This is due to the fact path must route to netlify's lambda.

---

## API endpoints

For the sake of comprehension let's say our `BASE_URL` is defined as follows : 

### Development

`BASE_URL` = `https//localhost:3000`

### Production

`BASE_URL` = `https//<DEPLOY_SITE_NAME>.netlify.app/.netlify/functions`

---

### Endpoint `BASE_URL` ( `/` )

#### Example url & response

`https://<BASE_URL>`

```json
{
  "title": "GITAPI PROJECT (draft)",
  "author": "https://multi.coop",
  "purpose": "Expose csv files hosted on a repo",
  "infos": "Backed with serverless-http, express, and Netlify lambda functions",
  "repo": "https://gitlab.com/multi-coop/experimental/gitapi-draft",
  "env": "development",
  "serverUrl": "http://localhost:3001",
  "meta": {
    "yamlFile": "./config-gitapi.yaml",
    "yaml": [
      {
        "file": "./data/csv/BERA/ANDORRE/hist.csv",
        "schema": "./data/json/bera-schema.json",
        "ignore": false,
        "options": {
          "separator": ","
        }
      },
      ...
    ]
  },
  "more": {
    "infos": "Discover the available datasets exposed with Gitapi on those links",
    "datasets": [
      {
        "index": 0,
        "name": "BERA-ANDORRE",
        "file": "./data/csv/BERA/ANDORRE/hist.csv",
        "dataset_index": 1,
        "url": "http://localhost:3001/v1?page=1&limit=3"
      },
      ...
    ]
  }
}
```

---

### Endpoint `/api`

#### Example url & response

`https://<BASE_URL>/api`

```json
{
  "help": "Here is the list of available datasets on the '/v1' endpoint",
  "available_datasets": [
    {
      "index": 0,
      "name": "BERA-ANDORRE",
      "url": "http://localhost:3000/v1/BERA-ANDORRE",
      "meta": {
        "source": {
          "src": "./data/csv/BERA/ANDORRE/hist.csv",
          "host": "statics",
          "sep": ",",
          "type": "csv"
        },
        "schema": {
          "src": "./data/json/bera-schema.json",
          "host": "statics",
          "type": "json"
        }
      }
    }
  ]
}
```

---

### Endpoint `/api/v1/:datasetName`

#### Request

| param     | type | in  | default | operators | definition | example |
| :---:     | :--: | :-: | :---:   | ---   | ---     | ---     |
| `datasetName` | str | params  | `'0'`    | -       | name of the dataset (according to config-gitapi.yaml, or dataset index) | `/v1/<datasetName>` | 
| `limit`       | int | query   | `1`      | -       | maximum number of items retrieved from dataset per page | `/v1/<datasetName>?limit=3` |
| `page`        | int | query   | `1`      | -       | page number | `/v1/<datasetName>?page=2` |
| `filter`      | str | query   | none     | `is` (default), `not`, `has`, `starts`, `ends`, `gt`, `gte`, `ls`, `lse` | filter dataset by field name and value | `/v1/<datasetName>?filter=<fieldName>:<operator>:<value>` <br> `/v1/<datasetName>?filter=<fieldName><value>` (with `is` as default operator)|
| `sortby`      | str | query   | none     | `asc` (default), `desc` | sorting results by field name | `/v1/<datasetName>?sort=<fieldName>:<operator (optional)>` |
| `calc`      | str | query   | none     | `sum`, `avg`, `max`, `min` | aggregation functions | `/v1/<datasetName>?calc=<fieldName>:<operator (optional)>` |
| `opt`         | str | query   | none     | `infos`, `schema`, `headers`, `request`, `meta` | results options | `/v1/<datasetName>?opt=<operator>:<show | hide>` |


##### Operators lexicon

| param    | operator | definition                 | usage                        | example                |
| :-:      | :-:      | ---                        | ---                          | ---                    | 
| `filter` | `is`     | Text is exactly            | `<fieldName>:is:<value>`     | `filter=name:is:barry` |
| `filter` | `not`    | Text is not                | `<fieldName>:not:<value>`    | `filter=name:not:barry`|
| `filter` | `has`    | Text contains              | `<fieldName>:has:<value>`    | `filter=name:has:barry`|
| `filter` | `starts` | Text starts with           | `<fieldName>:starts:<value>` | `filter=name:starts:barry`|
| `filter` | `ends`   | Text ends with             | `<fieldName>:ends:<value>`   | `filter=name:ends:barry`|
| `filter` | `gt`     | Greater than               | `<fieldName>:gt:<value>`     | `filter=amount:gt:barry`|
| `filter` | `gte`    | Greater or equal to        | `<fieldName>:gte:<value>`    | `filter=amount:gte:barry`|
| `filter` | `lt`     | Less than                  | `<fieldName>:lt:<value>`     | `filter=amount:lt:barry`|
| `filter` | `lte`    | Less or equal to           | `<fieldName>:lte:<value>`    | `filter=amount:lte:barry`|
| `sortby` | `asc`    | Sorts results - ascencing  | `<fieldName>:sortby:asc`     | `filter=name:sortby:asc`|
| `sortby` | `desc`   | Sorts results - descending | `<fieldName>:sortby:desc`    | `filter=name:sortby:desc`|
| `calc`   | `count`  | Count items from column    | `<fieldName>:count`          | `filter=amount:count`|
| `calc`   | `avg`    | Average value from column  | `<fieldName>:avg`            | `filter=amount:avg`|
| `calc`   | `sum`    | Sum of values from column  | `<fieldName>:sum`            | `filter=amount:sum`|
| `calc`   | `min`    | Minimum value from column  | `<fieldName>:min`            | `filter=amount:min`|
| `calc`   | `max`    | Maximum value from column  | `<fieldName>:max`            | `filter=amount:max`|
| `opt`    | `infos`   | Append infos (default: `show`)   | `infos:<show | hide>`   | `opt=infos:hide`|
| `opt`    | `schema`  | Append schema (default: `hide`)  | `schema:<show | hide>`  | `opt=schema:show`|
| `opt`    | `headers` | Append headers (default: `hide`) | `headers:<show | hide>` | `opt=headers:show`|
| `opt`    | `request` | Append request (default: `hide`) | `request:<show | hide>` | `opt=request:show`|
| `opt`    | `meta`    | Append meta (default: `show`)    | `meta:<show | hide>`    | `opt=meta:hide`|

#### Example url & response

`https://<BASE_URL>/v1/my-dataset?page=1&limit=3&filter=name:has:barry&sortby=amount`

```json
{
  "data": [ ... ],
  "infos": {
    "result": 3,
    "total": 100,
    "limit": 3,
    "pageNumber": 1,
    "totalPages": 34
  },
  "params": { 
    "datasetName": "my-dataset"
    },
  "query": {
    "page": "1",
    "limit": "3",
    "filter": "name:has:barry",
    "sortby": "amount"
  },
  "schema": [ ... ],
  "headers": { ... },
  "meta": { ... }
}
```

---

### Endpoint `/api/v1/:datasetName/:rowId`

#### Request

| Params    | type | in  | default | operators | definition | example |
| :---:     | :--: | :-: | :---:   | ---       | ---        | --- |
| `datasetName` | string  | params  | `'0'` | - | name of the dataset (according to config-gitapi.yaml, or dataset index) | `/v1/<datasetName>` | 
| `rowId` | int  | params  | `undefined` | - | row index, must be positive | `/v1/<datasetName>/<rowId>` | 
| `opt`         | str | query   | none     | `infos`, `schema`, `headers`, `request`, `meta` | results options | `/v1/<datasetName>?opt=<operator>:<show | hide>` |


#### Example url & response

`https://<BASE_URL>/v1/my-dataset/1`


```json
{
  "data": { ... },
  "infos": {
    "result": 1,
    "total": 100,
    "limit": 1
  },
  "params": { 
    "datasetName": "my-dataset",
    "rowId": "1",
   },
  "query": { },
  "schema": [ ... ],
  "headers": { ... },
  "meta": { ... }
}
```

---

## To do list

- [x] Add more selector args to main endpoints `/v1/:datasetName`, equivalent to : 
  - [x] `is:`	equals to $\to$ `filter=name:is:barry`
  - [x] `not:`	equals not $\to$ `filter=name:not:barry`
  - [x] `lt:`		less than $\to$ `filter=amount:lt:2`
  - [x] `lte:`	less than or equal $\to$ `filter=amount:lte:2`
  - [x] `gt:`		greater than $\to$ `filter=amount:gt:2`
  - [x] `gte:`	greater than or equal $\to$ `filter=amount:gte:2`
  - [x] `^:`		starts with, alias: `starts:` $\to$ `filter=name:starts:barr`
  - [x] `$:`		ends with, alias: `ends:` $\to$ `filter=name:ends:white`
  - [x] `has:`	contains text $\to$ `filter=name:has:barr`
    - [ ] `*:`		contains text, alias: `contains:` (a bit equivalent to previous)
- [x] Add aggregation args to main endpoints `/v1/:datasetName`, equivalent to : 
  - [x] `'$count':'*'`	counts the records in the data set $\to$ `calc=amount:count`
  - [x] `'$avg':'columnName'`	calculates the average of all numeric values in `columnName` $\to$ `calc=amount:avg`
  - [x] `'$sum':'columnName'`	calculates the sum of all numeric values in `columnName` $\to$ `calc=amount:sum` 
  - [x] `'$min':'columnName'`	returns the smallest of all numeric values in `columnName` $\to$ `calc=amount:min`
  - [x] `'$max':'columnName'`	returns the largest of all numeric values in `columnName` $\to$ `calc=amount:max`
- [ ] Add columns selectors args to main endpoints `/v1/:datasetName`, equivalent to : 
  - [ ] `'select':'columnName'`	returns results with only indicated columns
- [ ] Add results transformes args to main endpoints `/v1/:datasetName`, equivalent to :
  - [ ] `'as':'geojson'`	returns results formatted as geojson
- [x] Add options args to main endpoints `/v1/:datasetName`, equivalent to :
  - [x] `'opt':'show:infos'` append infos to response  $\to$ `opt=infos:show`
  - [x] `'opt':'show:schema'` append schema to response  $\to$ `opt=schema:show`
  - [x] `'opt':'show:headers'` append dataset headers to response  $\to$ `opt=headers:show`
  - [x] `'opt':'show:request'` append request to response  $\to$ `opt=request:show`
  - [x] `'opt':'show:meta'` append meta to response  $\to$ `opt=meta:show`
- [ ] Allow automatic iteration through data folder in config step
- [ ] Add swagger doc endpoint



---

## Notes & refs

- Pads : 
  - https://hackmd.io/0_Gj1bw4RIeTE0hZ88seVA?both
  - https://hackmd.io/@multi/r1OKfiN6O
- Inspired by : 
  - https://www.netlify.com/blog/2018/09/13/how-to-run-express.js-apps-with-netlify-functions/
  - https://awstip.com/express-server-on-netlify-for-free-step-by-step-guide-e5fbdb47d891
  - https://github.com/GRBgithub/netlify-express
  - https://github.com/neverendingqs/netlify-express
- "Cold start" problem of serverless apps :
  - https://dashbird.io/blog/can-we-solve-serverless-cold-starts/
  - https://builtin.com/software-engineering-perspectives/cold-starts-challenge-serverless-architecture
  - https://www.datadoghq.com/blog/serverless-cold-start-traces/

---

## Troubleshooting

- [ ] The `netlify-lambda` package is deprecated, we should use [netlify-cli](https://github.com/netlify/cli) instead.
- [ ] As a serverless function is stateless. Once deployed this could cause a "cold start" 500 error (`internal server error`), as mentionned in [this article](https://www.netlify.com/blog/2018/09/13/how-to-run-express.js-apps-with-netlify-functions/) : 
  - > By default Netlify's Lambda functions run with 128 MB memory and 10 seconds function execution limit. [...]
  - >  These limits mean you will not be able to have any large or long-running backend requests. If you bypass those limits, the Lambda function stops executing immediately with an error, which translates to a 500 HTTP status code. [...]
  - > When running serverless functions, AWS tries to reuse running function containers as best as they can but make no guarantees an instance of a function will be reused for multiple requests. This means you need to think about your functions as "stateless".
  - > This impacts how you need to handle any in-memory state or file system content to persist across requests.
  - For now we observed we only need to request once or twice the API before the server responds nicely...

---

### More about serverless functions

[![](http://img.youtube.com/vi/PCDhpRms4Ek/0.jpg)](http://www.youtube.com/watch?v=PCDhpRms4Ek "Up and running with serverless functions")


---

### Benchmark

Nice pre-existant similar solutions to Gitapi ([additional notes in this pad](https://hackmd.io/@multi/r1OKfiN6O)) : 

| Solution  | License    | Website              | Notes  |
| :-:       | :-:        | :-:                  | ---    |
| **Gitrows**   | MIT        | [Website](https://gitrows.com ) <br> [Repo](https://github.com/gitrows/gitrows) | Nodejs, a module (not service) |
| **csvapi**    | MIT        | [Explore.data.gouv](https://explore.data.gouv.fr/) <br> [Repo](https://github.com/etalab/csvapi) | Python, a module (wrapped in explore).data.gouv.fr |
| **Flat data** | MIT        | [Website](https://githubnext.com/projects/flat-data/) <br> [Repo](https://github.com/githubocto/flat) | Not active repo since 2021 | 
| **Datasette** | Apache 2.0 | [Website](https://datasette.io/) <br> [Repo](https://github.com/simonw/datasette) | |
| **Prose** | BSD 3 | [Repo](https://github.com/prose/prose) | |
| **Frictionless app** | - | [Presentation](https://docs.google.com/presentation/d/1VEVBCnxAouNXA0jEcyCjrXtx2R_t-QmFzSZCp3UlqD8) <br> [Open knowledge foundation](https://okfn.org/) |  |
| **Git-DMS** | - | [Repo](https://gitlab.com/jailbreak/git-dms) | Legacy project Jailbreak |


---

## Contact

You can reach us at <contact@multi.coop>

---

## License

Copyright © 2023, [multi](https://github.com/multi-coop).

[GNU AGPL 3.0](LICENSE)

